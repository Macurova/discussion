﻿namespace discussion.Enums
{
    /// <summary>
    /// Specify categories of topics.
    /// </summary>
    public enum ThemeCategory
    {
        /// <summary>
        /// Category for health topics.
        /// </summary>
        HEALTH = 0,
        /// <summary>
        /// Category for travel topics.
        /// </summary>
        TRAVEL,
        /// <summary>
        /// Category for politic topics.
        /// </summary>
        POLITICS,
        /// <summary>
        /// Category for technology topics.
        /// </summary>
        TECHNOLOGY,
        /// <summary>
        /// Category for culture topics.
        /// </summary>
        CULTURE,
        /// <summary>
        /// Category for psychlogy topics.
        /// </summary>
        PSYCHOLOGY,
        /// <summary>
        /// Category for hitory topics.
        /// </summary>
        HISTORY,
        /// <summary>
        /// Category for others.
        /// </summary>
        OTHER
    }
}
