﻿using System;

namespace discussion.Tool
{
    /// <summary>
    /// Reply to the comment.
    /// </summary>
    public class Reply : Response, IResponse
    {
        /// <summary>
        /// Initialization of the reply.
        /// </summary>
        public Reply() :
            base()
        { }

        /// <summary>
        /// Initialization of the reply.
        /// </summary>
        /// <param name="text">Context of reply.</param>
        /// <param name="username">Name of the user.</param>
        /// <param name="date">Date of creation.</param>
        public Reply(string text, string username, DateTime date) :
            base(text, username, date)
        { }

        /// <summary>
        /// Create reaction to comment, not implemented.
        /// </summary>
        /// <param name="text">Context of reply.</param>
        /// <param name="username">Name of the user who create reply.</param>
        /// <param name="date">Date of creation.</param>
        /// <returns>Instance of the reply to the reply.</returns>
        public IResponse React(string text, string username, DateTime date)
        {
            throw new NotImplementedException();
        }
    }
}
