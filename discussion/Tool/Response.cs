﻿using System;

namespace discussion.Tool
{
    /// <summary>
    /// Base model of response.
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Content of response.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Date of response creation.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Username of autor.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Initializes the response.
        /// </summary>
        public Response()
        {            
        }

        /// <summary>
        /// Initialization of the response.
        /// </summary>
        /// <param name="text">Context of discussion.</param>
        /// <param name="username">Name of the user.</param>
        /// <param name="date">Date of creation.</param>
        public Response(string text, string username, DateTime date)
        {
            Text = text;
            Author = username;
            Created = date;
        }
    }
}
