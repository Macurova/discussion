﻿using discussion.Models;
using System;
using System.Linq;

namespace discussion.Tool
{
    /// <summary>
    /// Serves the user context.
    /// </summary>
    public class UserContext
    {
        /// <summary>
        /// Flag indicate if the user is singed in.
        /// </summary>
        public bool Signed { get; set; } = false;

        /// <summary>
        /// Logged in user.
        /// </summary>
        public UserModel User { get; set; } = null;

        /// <summary>
        /// Gets the database context.
        /// </summary>
        public readonly DiscussionDbContext DBContext = new DiscussionDbContext();

        private static readonly UserContext instance = new UserContext();

        /// <summary>
        /// Gets the instance of the user context.
        /// </summary>
        /// <returns>Instance of the user context</returns>
        public static UserContext GetInstance()
        {
            return instance;
        }

        /// <summary>
        /// Creates discussion and save it to database, control if user is signed in.
        /// </summary>
        /// <param name="title">Title of the discussion.</param>
        /// <param name="text">Text of the discussion</param>
        /// <param name="theme">Topic of the discussion.</param>
        /// <returns>If the discussion was successfully created.</returns>
        public bool CreateDiscussion(string title, string text, ThemeModel theme)
        {
            if (Signed)
            {
                var discussion = new DiscussionModel()
                {
                    Title = title,
                    Text = text,
                    Theme = theme,
                    User = User,
                };
                DBContext.Discussions.Add(discussion);
                DBContext.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates comment and save it to database, control if user is signed in.
        /// </summary>
        /// <param name="discussion">Discussion which is commented</param>
        /// <param name="text">Text of the comment.</param>
        /// <returns>If the comment was successfully created.</returns>
        public bool CreateComment(DiscussionModel discussion, string text)
        {
            if (Signed)
            {
                var comment = new CommentModel()
                {
                    Discussion = discussion,
                    DiscussionID = discussion.ID,
                    Text = text,
                    User = User,
                    Created = DateTime.Now,
                };
                DBContext.Comments.Add(comment);
                DBContext.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates reply to the comment, control if user is signed in.
        /// </summary>
        /// <param name="comment">Comment which is replied.</param>
        /// <param name="text">Text of reply.</param>
        /// <returns>If the reply was successfully created.</returns>
        public bool CreateReply(CommentModel comment, string text)
        {
            if (Signed)
            {
                var reply = new ReplyModel()
                {
                    CommentID = comment.ID,
                    Comment = comment,
                    Text = text,
                    User = User,
                    Created = DateTime.Now,
                };
                DBContext.Replies.Add(reply);
                DBContext.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the context of the full discussion.
        /// </summary>
        /// <param name="id">Id of discussion.</param>
        /// <returns>Discussion context.</returns>
        public Discussion GetDiscussionWithReacts(int id)
        {
            var d = DBContext.Discussions.Where(x => x.ID == id).FirstOrDefault();
            var discussion = new Discussion
            {
                Author = d.User.UserName,
                Created = d.Created,
                Text = d.Text,
                Title = d.Title,
            };
            var comments = DBContext.Comments.Where(x => x.DiscussionID == id).ToList();
            foreach (var com in comments)
            {
                var comment = discussion.React(com.Text, com.User.UserName, com.Created);

                DBContext.Replies.Where(
                    x => x.CommentID == com.ID).ToList()
                    .ForEach(x => comment.React(x.Text, x.User.UserName, x.Created));
            }
            return discussion;
        }

    }
}
