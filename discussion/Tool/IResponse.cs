﻿using System;

namespace discussion.Tool
{
    /// <summary>
    /// Interface for instance thet create reaction. 
    /// </summary>
    public interface IResponse
    {
        /// <summary>
        /// Create instance of reaction.
        /// </summary>
        /// <param name="text">Reaction content.</param>
        /// <param name="username">Username of user who create reaction.</param>
        /// <param name="date">Date of creation.</param>
        /// <returns></returns>
        IResponse React(string text, string username, DateTime date);
    }
}
