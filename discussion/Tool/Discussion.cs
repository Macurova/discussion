﻿using System;
using System.Collections.Generic;

namespace discussion.Tool
{
    /// <summary>
    /// Discussion that contain list of comments and discussion info.
    /// </summary>
    public class Discussion : Response, IResponse
    {
        /// <summary>
        /// Initialization of the discussion.
        /// </summary>
        public Discussion() :
            base()
        { }

        /// <summary>
        /// Initialization of the discussion.
        /// </summary>
        /// <param name="text">Context of discussion.</param>
        /// <param name="username">Name of the user.</param>
        /// <param name="date">Date of creation.</param>
        public Discussion(string text, string username, DateTime date) :
            base(text, username, date)
        { }

        /// <summary>
        /// Title of the discussion.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// List of comments to the discussion.
        /// </summary>
        public List<Comment> Comments { get; set; } = new List<Comment>();

        /// <summary>
        /// Create reaction to discussion.
        /// </summary>
        /// <param name="text">Context of comment.</param>
        /// <param name="username">Name of the user who create comment.</param>
        /// <param name="date">Date of creation.</param>
        /// <returns>Instance of the comment to the discussion.</returns>
        public IResponse React(string text, string username, DateTime date)
        {
            var commment = new Comment(text, username, date);
            Comments.Add(commment);
            return commment;
        }
    }
}
