﻿using System;
using System.Collections.Generic;

namespace discussion.Tool
{
    /// <summary>
    /// Comment to the discussion that contain list of reply.
    /// </summary>
    public class Comment : Response, IResponse
    {
        /// <summary>
        /// Initialization of the comment.
        /// </summary>
        /// <param name="text">Context of comment.</param>
        /// <param name="username">Name of the user.</param>
        /// <param name="date">Date of creation.</param>
        public Comment(string text, string username, DateTime date) :
            base(text, username, date)
        { }

        // <summary>
        /// Initialization of the comment.
        /// </summary>
        public Comment() :
           base()
        { }

        /// <summary>
        /// List of replies to the comment.
        /// </summary>
        public List<Reply> Replies { get; set; } = new List<Reply>() { };

        /// <summary>
        /// Create reaction to comment.
        /// </summary>
        /// <param name="text">Context of reply.</param>
        /// <param name="username">Name of the user.</param>
        /// <param name="date">Date of creation.</param>
        /// <returns>Instance of the reply to the comment.</returns>
        public IResponse React(string text, string username, DateTime date)
        {
            var instance = new Reply(text, username, date);
            Replies.Add(instance);
            return instance;
        }
    }
}
