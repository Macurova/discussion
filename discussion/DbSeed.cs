﻿using discussion.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace discussion
{
    /// <summary>
    /// Database initialization.
    /// </summary>
    public static class DbSeed
    {
        /// <summary>
        /// Add data to tables if there is no user.
        /// </summary>
        /// <param name="context">Database context.</param>
        public static void Initialize(DiscussionDbContext context)
        {
            context.Database.Migrate();
            if (context.Users.ToList().Count > 0)
                return;
            Delete(context);
            SeedUsers(context);
            SeedThemes(context);
            SeedDiscussion(context);
            SeedComment(context);
            SeedReply(context);
        }

        /// <summary>
        /// Deletes datas from all tables.
        /// </summary>
        /// <param name="context">Database context</param>
        public static void Delete(DiscussionDbContext context)
        {
            context.Database.EnsureCreated();

            context.RemoveRange(context.Replies);
            context.RemoveRange(context.Comments);
            context.RemoveRange(context.Discussions);
            context.RemoveRange(context.Themes);
            context.RemoveRange(context.Users);
            context.SaveChanges();
        }


        private static void SeedThemes(DiscussionDbContext context)
        {
            var themes = new List<ThemeModel>
            {
                new ThemeModel()
                {
                    Category = Enums.ThemeCategory.HEALTH,
                    Name = "Covid-19"
                },
                new ThemeModel()
                {
                    Category = Enums.ThemeCategory.HEALTH,
                    Name = "Obesity"
                },
                new ThemeModel()
                {
                    Category = Enums.ThemeCategory.TRAVEL,
                    Name = "Italy"
                },
                new ThemeModel()
                {
                    Category = Enums.ThemeCategory.TRAVEL,
                    Name = "Food in Italy"
                },
                new ThemeModel()
                {
                    Category= Enums.ThemeCategory.TECHNOLOGY,
                    Name = "Programming"
                },
                new ThemeModel()
                {
                    Category= Enums.ThemeCategory.TECHNOLOGY,
                    Name = "Robotics"
                },
                new ThemeModel()
                {
                    Category= Enums.ThemeCategory.HISTORY,
                    Name = "Second world war"
                },
            };

            context.Themes.AddRange(themes);
            context.SaveChanges();
        }


        private static void SeedDiscussion(DiscussionDbContext context)
        {
            var user = context.Users.Where(p => p.UserName == "anna").FirstOrDefault();
            var theme = context.Themes.Where(p => p.Name == "Covid-19").FirstOrDefault();


            var userDavid = context.Users.Where(p => p.UserName == "david").FirstOrDefault();
            var themeTechnology = context.Themes.Where(p => p.Name == "Programming").FirstOrDefault();


            var discussions = new List<DiscussionModel>
            {

                new DiscussionModel()
                {
                    User = user,                    
                    Theme = context.Themes.Where(p => p.Name == "Obesity").FirstOrDefault(),
                    Title = "Obesity in USA",
                    Text = "What percentage of the US population is overly obese and what percentage of the population wants to do something about it?"
                },
                 new DiscussionModel()
                 {
                     User = user,
                    Theme = theme,
                    Title = "Covid-19 in Italy",
                    Text = "What measures are being taken against the spread of the covid virus in Italy?"
                 },
                 new DiscussionModel()
                 {
                     User = userDavid,
                    Theme = themeTechnology,
                    Title = "Read file in Python",
                    Text = "What is the best way to open and read the file in Python?"
                 }
            };

            context.Discussions.AddRange(discussions);
            context.SaveChanges();
        }


        private static void SeedComment(DiscussionDbContext context)
        {
            var user = context.Users.Where(p => p.UserName == "elza").FirstOrDefault();
            var d1 = context.Discussions.Where(p => p.Title == "Covid-19 in Italy").FirstOrDefault();
            var d2 = context.Discussions.Where(p => p.Title == "Read file in Python").FirstOrDefault();


            var user2 = context.Users.Where(p => p.UserName == "peter").FirstOrDefault();
            var themeTechnology = context.Themes.Where(p => p.Name == "Programming").FirstOrDefault();


            var comments = new List<CommentModel>
            {
                new CommentModel()
                {
                    Created = DateTime.Parse("2019-12-06"),
                    DiscussionID = d1.ID,
                    Discussion = d1,
                    User = user,
                    Text = "Look at http://www.italia.it/en/useful-info/covid-19-updates-information-for-tourists.html"
                },
                new CommentModel()
                {
                    Created = DateTime.Parse("2019-12-06"),
                    DiscussionID = d1.ID,
                    Discussion = d1,
                    User = user,
                    Text = "Respirators everywhere" },
                 new CommentModel()
                {
                    Created = DateTime.Parse("2018-12-28"),
                    DiscussionID = d2.ID,
                     Discussion = d2,
                    User = user2,
                    Text = "with open('file path', 'r') as f:\n data = f.read()\n"
                 },
                 new CommentModel()
                {
                    Created = DateTime.Parse("2018-12-29"),
                    DiscussionID = d2.ID,
                    Discussion = d2,
                    User = user,
                    Text = "Use: file = open('filename', 'r') \n for line in file:\n ##do stg with line\n " +
                    "or \nlines = [line for line in open('filename')]\n " +
                    "If file is huge, read() or readlines() is bad idea, as it loads (without size parameter), whole file into memory.\n"
                 },
            };

            context.Comments.AddRange(comments);
            context.SaveChanges();
        }

        private static void SeedReply(DiscussionDbContext context)
        {
            var user = context.Users.Where(p => p.UserName == "david").FirstOrDefault();
            var user2 = context.Users.Where(p => p.UserName == "peter").FirstOrDefault();
            var comment = context.Comments.Where(p => p.Text == "Respirators everywhere").FirstOrDefault();


            var reply = new List<ReplyModel>
            {
                new ReplyModel()
                {
                    Created = DateTime.Parse("2019-12-06"),
                    CommentID = comment.ID,
                    User = user,
                    Text = "Maybe you should have a vaccination certificate ...",
                    Comment = comment,
                },
                new ReplyModel()
                {
                    Created = DateTime.Parse("2019-12-06"),
                    CommentID = comment.ID,
                    User = user2,
                    Text = "Keep the distance",
                    Comment = comment
                },
            };

            context.Replies.AddRange(reply);
            context.SaveChanges();
        }

        private static void SeedUsers(DiscussionDbContext context)
        {
            var users = new List<UserModel>
            {
                new UserModel()
                {
                    UserName = "anna",
                    Email = "anna@stg.com",
                    Password = "anna"
                },
                new UserModel()
                {
                    UserName = "peter",
                    Email = "peter@stg.com",
                    Password = "peter"
                },
                new UserModel()
                {
                    UserName = "david",
                    Email = "david@stg.com",
                    Password = "david"
                },
                new UserModel()
                {
                    UserName = "elza",
                    Email = "elza@stg.com",
                    Password = "elza"
                },
            };

            context.Users.AddRange(users);
            context.SaveChanges();
        }
    }
}
