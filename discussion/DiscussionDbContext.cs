﻿using discussion.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace discussion
{
    /// <summary>
    /// Serves database context.
    /// </summary>
    public class DiscussionDbContext : DbContext
    {
        /// <summary>
        /// Sets options for builder during configuration.
        /// </summary>
        /// <param name="optionsBuilder">Builder options.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("User ID = postgres;Password=heslo;Server=localhost;Port=5432;Database=discussion;Integrated Security=true; Pooling=true;");
        }

        /// <summary>
        /// Configuration for entities and relations.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CommentModel>()
                .Property(p => p.ID)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<ReplyModel>()
                .Property(p => p.ID)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<DiscussionModel>()
                .Property(p => p.ID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<UserModel>()
                .Property(p => p.ID)
                .ValueGeneratedOnAdd();

            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Modify behavior durin save to DB: Add datetime on creation.
        /// Source: https://www.entityframeworktutorial.net/faq/set-created-and-modified-date-in-efcore.aspx
        /// </summary>
        public override int SaveChanges()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseModel && (
                        e.State == EntityState.Added));

            foreach (var entityEntry in entries)
            {
                ((BaseModel)entityEntry.Entity).Created = DateTime.Now;
            }

            return base.SaveChanges();
        }

        /// <summary>
        /// Works with the DB model of theme.
        /// </summary>
        public DbSet<ThemeModel> Themes { get; set; }


        /// <summary>
        /// Works with the DB model of discussion.
        /// </summary>
        public DbSet<DiscussionModel> Discussions { get; set; }

        /// <summary>
        /// Works with the DB model of comment.
        /// </summary>
        public DbSet<CommentModel> Comments { get; set; }

        /// <summary>
        /// Works with the DB model of reply.
        /// </summary>
        public DbSet<ReplyModel> Replies { get; set; }

        /// <summary>
        /// Works with the DB model of user.
        /// </summary>
        public DbSet<UserModel> Users { get; set; }
    }
}