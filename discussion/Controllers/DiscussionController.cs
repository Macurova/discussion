﻿using discussion.Models;
using discussion.Tool;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace discussion.Controllers
{
    /// <summary>
    /// Controller for discusion data, use DBContext.
    /// </summary>
    public class DiscussionController : ControllerBase
    {
        private readonly DiscussionDbContext _applicationDbContext;

        private readonly UserContext _userContext;

        /// <summary>
        /// Initialize instance.
        /// </summary>
        /// <param name="userContext">User context.</param>
        public DiscussionController(UserContext userContext)
        {
            _applicationDbContext = userContext.DBContext;
            _userContext = userContext;
        }

        /// <summary>
        /// Get discussion list.
        /// </summary>
        [HttpGet("discussions")]
        public IEnumerable<DiscussionModel> Get()
        {
            return _applicationDbContext.Discussions.ToList();
        }

        /// <summary>
        /// Get discussion list.
        /// </summary>
        /// <param name="id">Unique discussion id.</param>
        [HttpGet("discussion/{id}")]
        public DiscussionModel GetDiscussion(int id)
        {
            return _applicationDbContext.Discussions.Where(x => x.ID == id).FirstOrDefault();
        }


        /// <summary>
        /// Create discussion with specific theme.
        /// </summary>
        /// <param name="title">Discussion title.</param>
        /// <param name="text">Discussion text</param>
        /// <param name="themeId">Unique theme id.</param>
        /// <response code="200">Discussion successfully created.</response> 
        /// <response code="404">Theme does not exist.</response> 
        /// <response code="405">Method is not allowed, to create discussion the user must be logged in.</response> 
        [HttpPost, Route("discussion/{themeId}")]
        public IActionResult Post(int themeId, string title, string text)
        {
            var theme = _applicationDbContext.Themes.Where(x => x.ID == themeId).FirstOrDefault();
            if (theme != null)
            {
                if (_userContext.CreateDiscussion(title, text, theme))
                {
                    return Ok();
                }
                return StatusCode(StatusCodes.Status405MethodNotAllowed, "To create discussion the user must be logged in.");
            }
            return NotFound("Theme does not exist.");
        }

        /// <summary>
        /// Gets the context of the full discussion.
        /// </summary>
        /// <param name="id">Id of discussion.</param>
        /// <returns>Discussion context.</returns>
        [HttpGet, Route("discussion/{id}/context")]
        public Discussion GetDiscussionWithReacts(int id)
        {
            return _userContext.GetDiscussionWithReacts(id);
        }
    }
}
