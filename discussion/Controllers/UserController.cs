﻿using discussion.Models;
using discussion.Tool;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace discussion.Controllers
{
    /// <summary>
    /// Controller for user data, use DBContext.
    /// </summary>
    public class UserController : ControllerBase
    {

        private readonly DiscussionDbContext _applicationDbContext;
        private readonly UserContext _userContext;

        /// <summary>
        /// Initialize instance.
        /// </summary>
        /// <param name="userContext">User context.</param>
        public UserController(UserContext userContext) : base()
        {
            _applicationDbContext = userContext.DBContext;
            _userContext = userContext;
        }

        /// <summary>
        /// Log in for registered users.
        /// </summary>
        /// <param name="username">Unique user name.</param>
        /// <param name="password">User password.</param>
        /// <response code="200">User successfully logged.</response> 
        /// <response code="403">Bad username or password.</response> 

        [HttpPost("login")]
        public IActionResult Login(string username, string password)
        {
            if (_userContext.Signed)
            {
                Forbid("User " + _userContext.User.UserName + " already logged in.");
            }
            var user = _applicationDbContext.Users.Where(x => x.UserName == username).FirstOrDefault();
            if (user != null)
            {
                if (password == user.Password)
                {
                    _userContext.Signed = true;
                    _userContext.User = user;
                    return Ok();
                }
                return Forbid("Bad username or password.");
            }
            return NotFound("Username does not found. User is not registered yet.");
        }

        /// <summary>
        /// Register the new user.
        /// </summary>
        /// <param name="username">Unique username.</param>
        /// <param name="email">Specific email is not controlled.</param>
        /// <param name="password"></param>
        /// <response code="200">User successfully registered.</response>
        /// <response code="400">Invalid email.</response> 
        /// <response code="403">Username already exist.</response> 

        [HttpPost("register")]
        public IActionResult Register(string username, string email, string password)
        {
            if (!new EmailAddressAttribute().IsValid(email))
                return BadRequest("Invalid email");
            var user = _applicationDbContext.Users.Where(x => x.UserName == username).FirstOrDefault();
            if (user == null)
            {
                user = new UserModel()
                {
                    Email = email,
                    UserName = username,
                    Password = password
                };
                _applicationDbContext.Users.Add(user);
                _applicationDbContext.SaveChanges();
                return Ok();
            }
            return NotFound("Username is already registered.");
        }


        /// <summary>
        /// Log out the user.
        /// </summary>
        /// <response code="200">User successfully logged out.</response>
        /// <response code="400">Invalid email.</response> 
        /// <response code="405">Not allowed, user is not logeg in.</response> 
        [HttpPost("logout")]
        public IActionResult LogOut()
        {
            if (_userContext.Signed)
            {
                _userContext.Signed = false;
                _userContext.User = null;
                return Ok();
            }
            return StatusCode(StatusCodes.Status405MethodNotAllowed);
        }

        /// <summary>
        /// Get list of users.
        /// </summary>
        [HttpGet, Route("users")]
        public IEnumerable<UserModel> GetUsers()
        {
            return _applicationDbContext.Users.ToList();
        }

        /// <summary>
        /// Change password of logged user.
        /// </summary>
        /// <param name="password">Current password.</param>
        /// <param name="newPassword">New changed password.</param>
        /// <response code="200">Successfully changed the password.</response>
        /// <response code="400">Bad password.</response> 
        /// <response code="405">Not allowed, user is not logeg in.</response> 
        [HttpPut("user/change_password")]
        public IActionResult ChangePassword([Required] string password, [Required] string newPassword)
        {
            if (_userContext.Signed)
            {
                if (_userContext.User.Password == password)
                {
                    _userContext.User.Password = newPassword;
                    _applicationDbContext.Users.Update(_userContext.User);
                    _applicationDbContext.SaveChanges();
                    return Ok();
                }
                return BadRequest("Bad password.");
            }
            return StatusCode(StatusCodes.Status405MethodNotAllowed, "To change the password, the user must be logged in.");
        }

        /// <summary>
        /// Register the new user.
        /// </summary>
        /// <param name="username">Unique username.</param>
        /// <response code="200">User successfully registered.</response>
        /// <response code="400">Invalid email.</response> 
        /// <response code="403">Username already exist.</response> 
        [HttpPut("user/edit/{username}")]
        public IActionResult ChangeUsername(string username)
        {
            if (_userContext.Signed)
            {
                var user = _applicationDbContext.Users.Where(x => x.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    _userContext.User.UserName = username;
                    _applicationDbContext.Users.Update(_userContext.User);
                    _applicationDbContext.SaveChanges();
                    return Ok();
                }
                return Forbid("Username already exist.");
            }
            return StatusCode(StatusCodes.Status405MethodNotAllowed, "To change the username, the user must be logged in.");
        }
    }

}
