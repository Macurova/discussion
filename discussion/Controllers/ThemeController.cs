﻿using discussion.Enums;
using discussion.Models;
using discussion.Tool;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace discussion.Controllers
{
    /// <summary>
    /// Controller for theme data, use DBContext.
    /// </summary>
    public class ThemeController : Controller
    {
        private readonly UserContext _userContext;

        /// <summary>
        /// Initialize instance.
        /// </summary>
        /// <param name="userContext">User context.</param>
        public ThemeController(UserContext userContext)
        {
            _userContext = userContext;
        }

        /// <summary>
        /// Create theme.
        /// </summary>
        /// <param name="name">Name of theme.</param>
        /// <param name="category">Category of theme.</param>
        /// <response code="200">Theme successfully created.</response> 
        [HttpPost, Route("theme")]
        public IActionResult Post(string name, ThemeCategory category)
        {
            var theme = new ThemeModel
            {
                Name = name,
                Category = category
            };

            _userContext.DBContext.Themes.Add(theme);
            _userContext.DBContext.SaveChanges();
            return Ok();
        }


        /// <summary>
        /// Get list of theme.
        /// </summary>
        /// <returns>List of themes.</returns>
        [HttpGet, Route("themes")]
        public IEnumerable<ThemeModel> Get()
        {           
            return _userContext.DBContext.Themes.ToArray();
        }
    }
}
