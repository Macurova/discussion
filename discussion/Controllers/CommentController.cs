﻿using discussion.Models;
using discussion.Tool;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace discussion.Controllers
{
    /// <summary>
    /// Controller for comment and reply data, use DBContext.
    /// </summary>
    public class CommentController : Controller
    {
        private readonly UserContext _userContext;

        /// <summary>
        /// Initialize instance.
        /// </summary>
        /// <param name="userContext">User context.</param>
        public CommentController(UserContext userContext)
        {
            _userContext = userContext;
        }

        /// <summary>
        /// Create comment to specific discussion.
        /// </summary>
        /// <param name="discussionId">Unique discussion id.</param>
        /// <param name="text">Comment text.</param>
        /// <response code="200">Comment successfully created.</response> 
        /// <response code="404">Discussion does not exist.</response> 
        /// <response code="405">Method is not allowed, to create comment the user must be logged in.</response> 
        [HttpPost, Route("discussion/{discussionId}/comment")]
        public IActionResult PostComment(int discussionId, string text)
        {
            var discussion = _userContext.DBContext.Discussions.Where(x => x.ID == discussionId).FirstOrDefault();
            if (discussion != null)
            {
                if (_userContext.CreateComment(discussion, text))
                {
                    return Ok();
                }
                return StatusCode(StatusCodes.Status405MethodNotAllowed, "To create comment the user must be logged in.");
            }
            return NotFound("Discussion not exist");
        }


        /// <summary>
        /// Get comments to specific discussion.
        /// </summary>
        /// <param name="discussionId">Unique discussion id.</param>
        /// <returns>List of comments.</returns>
        [HttpGet, Route("discussion/{discussionId}/comments")]
        public IEnumerable<CommentModel> GetComments(int discussionId)
        {
            return _userContext.DBContext.Comments.Where(x => x.DiscussionID == discussionId).ToList();
            
        }


        /// <summary>
        /// Create reply the specific commment for in discussion.
        /// </summary>
        /// <param name="commentId">Unique comment id.</param>
        /// <param name="text">Reply text.</param>
        /// <response code="200">Reply successfully created.</response> 
        /// <response code="404">Comment does not exist.</response> 
        /// <response code="405">Method is not allowed, to create reply the user must be logged in.</response> 
        [HttpPost, Route("comment/{commentId}/reply")]
        public IActionResult Post(int commentId, string text)
        {
            var comment = _userContext.DBContext.Comments.Where(x => x.ID == commentId).FirstOrDefault();
            if (comment != null)
            {
                if (_userContext.CreateReply(comment, text))
                {
                    return Ok();
                }
                return StatusCode(StatusCodes.Status405MethodNotAllowed, "To create comment the user must be logged in.");
            }
            return NotFound("Comment not exist");
        }

        /// <summary>
        /// Get replies to the specific comment.
        /// </summary>
        /// <param name="commentId">Unique comment id.</param>
        /// <returns>List of comments.</returns>
        [HttpGet, Route("comment/{commentId}/replies")]
        public List<ReplyModel> GetReplies(int commentId)
        {
            return _userContext.DBContext.Replies.Where(x => x.CommentID == commentId).ToList();

        }
    }
}
