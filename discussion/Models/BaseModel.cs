﻿using System;
using System.ComponentModel.DataAnnotations;

namespace discussion.Models
{
    /// <summary>
    /// Base Data Model
    /// </summary>
    public class BaseModel
    {
        /// <summary>
        /// Id of model.
        /// </summary>
        [Key]
        [Required]
        public int ID { get; set; }

        /// <summary>
        /// Gets/Sets text.
        /// </summary>

        [Required]
        public string Text { get; set; }

        /// <summary>
        /// Date of model creation.
        /// </summary>

        public DateTime Created { get; set; }

        /// <summary>
        /// The user who created the object.
        /// </summary>
        public virtual UserModel User { get; set; }
    }
}
