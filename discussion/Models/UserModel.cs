﻿using System.ComponentModel.DataAnnotations;

namespace discussion.Models
{
    /// <summary>
    /// Model of the user.
    /// </summary>
    public class UserModel
    {
        /// <summary>
        /// Identification of the user.
        /// </summary>
        [Key]
        [Required]
        public int ID { get; set; }

        /// <summary>
        /// Unique name of the user.
        /// </summary>
        [Required]
        public string UserName { get; set; }

        /// <summary>
        /// User password.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// User email.
        /// </summary>
        [Required]
        public string Email { get; set; }

    }
}
