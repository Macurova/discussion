﻿using discussion.Enums;
using System.ComponentModel.DataAnnotations;

namespace discussion.Models
{
    /// <summary>
    /// Model of the specific theme.
    /// </summary>
    public class ThemeModel
    {
        /// <summary>
        /// Identifier of the theme.
        /// </summary>
        [Key]
        [Required]
        public int ID { get; set; }

        /// <summary>
        /// Theme category.
        /// </summary>
        public ThemeCategory Category { get; set; }

        /// <summary>
        /// Name of the theme.
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
