﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace discussion.Models
{
    /// <summary>
    /// Model of the discussion.
    /// </summary>
    public class DiscussionModel : BaseModel
    {
        /// <summary>
        /// Title of the discussion.
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Theme of the discussion.
        /// </summary>
        public virtual ThemeModel Theme { get; set; }

        /// <summary>
        /// List of comments to the discussion.
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<CommentModel> Comments { get; set; }

    }
}
