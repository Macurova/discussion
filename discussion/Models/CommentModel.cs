﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace discussion.Models
{
    /// <summary>
    /// Model of the comment to specific discusion.
    /// </summary>
    public class CommentModel : BaseModel
    {
        /// <summary>
        /// Discusion identifier.
        /// </summary>
        [Required]
        public int DiscussionID { get; set; }

        /// <summary>
        /// List of replies to the comment.
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<ReplyModel> Replies { get; set; }

        /// <summary>
        /// Discussion model.
        /// </summary>
        [JsonIgnore]
        public virtual DiscussionModel Discussion { get; set; }

    }
}
