﻿using System.ComponentModel.DataAnnotations;

namespace discussion.Models
{
    /// <summary>
    /// Model of the reply to the comment.
    /// </summary>
    public class ReplyModel : BaseModel
    {
        /// <summary>
        /// Comment identification.
        /// </summary>
        [Required]
        public int CommentID { get; set; }

        /// <summary>
        /// Model of the comment.
        /// </summary>        
        public virtual CommentModel Comment { get; set; }

    }
}
