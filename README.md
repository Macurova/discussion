# Discussion REST API

Backend for simple discussion works with PostgreDB in .NET.

Database configuration is in class DiscussionDbContext. 

Project use database named 'discussion'.


## Run the project

Run project: 	` dotnet run ` in 'discussion' folder

Use commands:  	`dotner ef migrations add Name`,   `dotnet ef database update`


## Dependency

.NET SDK
- TargetFramework: netcoreapp3.1

PostgreSQL
